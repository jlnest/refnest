Ref.Nest
===

## Introduction

### Purpose
- 為簡化查詢pubmed資料庫
- 定義專案檔案查詢格式標準


### Functions

- PubMed查詢程式
- MeSH 查詢程式
- MeSH 離線資料庫
- 查詢紀錄

## Data Resource

- [E-utilities Quick Start, NCBI](https://www.ncbi.nlm.nih.gov/books/NBK25500/)
- [MeSH Data (Medical Subject Headings)](https://www.nlm.nih.gov/databases/download/mesh.html)


## Storage

- CSV
- PostgreSQL Server
- [Elasticsearch](https://www.elastic.co/)

## Platform

### Backen 

- python 3.*
- Django 2.*

### Front-end

- React.js 

